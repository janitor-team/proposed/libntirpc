Source: ntirpc
Section: net
Priority: optional
Maintainer: Dmitry Smirnov <onlyjob@debian.org>
Uploaders: Christoph Martin <martin@uni-mainz.de>
Build-Depends: debhelper (>= 9),
  cmake,
  libkrb5-dev,
  liburcu-dev,
Standards-Version: 4.3.0
Homepage: https://github.com/nfs-ganesha/ntirpc
Vcs-Git: https://salsa.debian.org/debian/libntirpc.git
Vcs-Browser: https://salsa.debian.org/debian/libntirpc

Package: libntirpc3.4
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}, libtirpc-common
Description: new transport-independent RPC library
 Port of Sun's transport-independent RPC library to Linux. The library
 is intended as a replacement for the RPC code in the GNU C library,
 providing among others support for RPC (and in turn, NFS) over IPv6.
 .
 Changes introduced in the ntirpc library include:
  * Bi-directional operation.
  * Full-duplex operation on the TCP (vc) transport.
  * Thread-safe operating modes:
    * new locking primitives and lock callouts (interface change).
    * stateless send/recv on the TCP transport (interface change).
  * Flexible server integration support.
  * Event channels.

Package: libntirpc-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libntirpc3.4 (= ${binary:Version}),
Description: new transport-independent RPC library - development files
 Port of Sun's transport-independent RPC library to Linux. The library
 is intended as a replacement for the RPC code in the GNU C library,
 providing among others support for RPC (and in turn, NFS) over IPv6.
 .
 Changes introduced in the ntirpc library include:
  * Bi-directional operation.
  * Full-duplex operation on the TCP (vc) transport.
  * Thread-safe operating modes:
    * new locking primitives and lock callouts (interface change).
    * stateless send/recv on the TCP transport (interface change).
  * Flexible server integration support.
  * Event channels.
 .
 This package provides development files.
